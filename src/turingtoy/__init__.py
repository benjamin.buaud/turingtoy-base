from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    tape = list(input_)
    head_position = 0
    current_state = machine["start state"]
    history = [(current_state, tape.copy(), head_position)]
    execution_order = []
    execution_order.append(current_state)
    final_states = machine["final states"]
    halted = False

    def do_transition(transition):
        nonlocal current_state, tape, head_position
        if isinstance(transition, dict):
            new_state = transition.get("L", current_state)
            new_symbol = transition.get("write", tape[head_position])
            move = transition.get("R", "N")
        elif isinstance(transition, str):
            new_state = current_state
            new_symbol = tape[head_position]
            move = transition

        current_state = new_state
        tape[head_position] = new_symbol

        if move == "L":
            head_position -= 1
        elif move == "R":
            head_position += 1

    while steps is None or steps > 0:
        current_symbol = tape[head_position]
        if current_symbol not in machine["table"][current_state]:
            halted = True
            break

        transition = machine["table"][current_state][current_symbol]
        do_transition(transition)

        history.append((current_state, tape.copy(), head_position))

        if current_state in final_states:
            halted = True
            break

    result = "".join(tape).strip(machine["blank"])
    return result, history, halted
